var header, logo, search, userActivity, userAccount;

$(document).ready(function() {
    /*Get header elements*/
    header = $('header');
    logo = $('.logo');
    search = $('.search');
    userActivity = $('.user-activity');
    userAccount = $('.user-account');
    setSearchWidth();
});

$( window ).resize(function() {
    setSearchWidth();
});

/*Function set search width to maximum possible*/
function setSearchWidth(){
    //Get other header components width
    var headerWidth = header.width();
    var logoWidth = logo.width();
    var userActivityWidth = userActivity.width();
    var userAccountWidth = userAccount.width();
    //Get space between them
    var searchMargins = parseInt(search.css('margin-left')) + parseInt(search.css('margin-right'));
    var userAccountMargins = parseInt(userAccount.css('margin-left'));
    
    //Calculate maximum width to search
    var searchWidth =parseInt(headerWidth - logoWidth - userActivityWidth -userAccountWidth - searchMargins - userAccountMargins - 20);
    search.css('width', searchWidth);
}

